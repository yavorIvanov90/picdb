﻿using System;
using MetaData;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SWE_2;
using SWE_2.Model;
using static SWE_2.MainWindow;
using SWE_2.ViewModel;
using System.Collections.Generic;
using Moq;
using System.Collections.ObjectModel;

namespace UnitTests
{
    [TestClass]
    public class UnitTest
    {

        // private readonly ILogger _logger;
        private readonly IConfiguration _conf;
        private readonly MainWindowViewModel _viewModel;
        private readonly MySqlConnection _sqlConn;
        private readonly PhotographerViewModel _pViewModel;
        private readonly PictureViewModel _picViewModel;

        private readonly string connection = "user=swp;password=technikumwien;server=localhost;database=photo_app;port=3306;";

        public UnitTest()
        {
            var _logger = new Mock<ILogger<MySqlConnection>>().Object;
            _sqlConn = new MySqlConnection(_logger);
            var _logger2 = new Mock<ILogger<PhotographerViewModel>>().Object;
            _sqlConn.Connection = connection;
            _sqlConn.openConnection();
            _pViewModel = new PhotographerViewModel(_sqlConn, _logger2);
            _picViewModel = new PictureViewModel(_sqlConn, _pViewModel);
        }

        [TestMethod]
        public void TestIPTCMetaDataTitle()
        {
            IPTCMetaData meta = new IPTCMetaData();
            meta.Title = "Test";
            Assert.AreEqual("Test", meta.Title);
        }

        [TestMethod]
        public void TestIPTCMetaDataDescription()
        {
            IPTCMetaData meta = new IPTCMetaData();
            meta.Description = "Description";
            Assert.AreEqual("Description", meta.Description);
        }

        [TestMethod]
        public void TestIPTCMetaDataCity()
        {
            IPTCMetaData meta = new IPTCMetaData();
            meta.City = "City";
            Assert.AreEqual("City", meta.City);
        }

        [TestMethod]
        public void TestEXIFMetaDataManufacturer()
        {
            EXIFMetaData meta = new EXIFMetaData();
            meta.Manufacturer = "CANON";
            Assert.AreEqual("CANON", meta.Manufacturer);
        }

        [TestMethod]
        public void TestEXIFMetaDataFlash()
        {
            EXIFMetaData meta = new EXIFMetaData();
            meta.Flash = "off";
            Assert.AreEqual("off", meta.Flash);
        }

        [TestMethod]
        public void TestEXIFMetaDataModel()
        {
            EXIFMetaData meta = new EXIFMetaData();
            meta.Model = "DS3200";
            Assert.AreEqual("DS3200", meta.Model);
        }
        [TestMethod]
        public void TestEXIFMetaDataSoftware()
        {
            EXIFMetaData meta = new EXIFMetaData();
            meta.Software = "2.3.1";
            Assert.AreEqual("2.3.1", meta.Software);
        }

        [TestMethod]
        public void TestEXIFMetaColorSpace()
        {
            EXIFMetaData meta = new EXIFMetaData();
            meta.ColorSpace = "sRGB";
            Assert.AreEqual("sRGB", meta.ColorSpace);
        }

        [TestMethod]
        public void TestPicturePath()
        {
            string path = "E:/Google Drive/FH/4 Semester/SWE/Uebung2/SWE_2/bin/Debug/Gallery/DSC_0022.JPG";
            Picture picture = new Picture(path);
            Assert.AreEqual(path, picture.Path);
        }

        [TestMethod]
        public void TestPictureStream()
        {
            string path = "E:/Google Drive/FH/4 Semester/SWE/Uebung2/SWE_2/bin/Debug/Gallery/DSC_0022.JPG";
            Picture picture = new Picture(path);
            Assert.IsNotNull(picture.Stream);
        }

        [TestMethod]
        public void TestPictureID()
        {
            string path = "E:/Google Drive/FH/4 Semester/SWE/Uebung2/SWE_2/bin/Debug/Gallery/DSC_0022.JPG";
            Picture picture = new Picture(path);
            picture.Id = 213;
            Assert.AreEqual(213, picture.Id);
        }

        [TestMethod]
        public void TestPictureTags()
        {
            string path = "E:/Google Drive/FH/4 Semester/SWE/Uebung2/SWE_2/bin/Debug/Gallery/DSC_0022.JPG";
            Picture picture = new Picture(path);
            List<string> tags = new List<string>();
            tags.Add("tag1");
            tags.Add("tag2");
            tags.Add("tag2");
            picture.Tags = tags;
            Assert.AreEqual(tags, picture.Tags);
        }

        [TestMethod]
        public void TestPicturIPTCMetaData()
        {
            string path = "E:/Google Drive/FH/4 Semester/SWE/Uebung2/SWE_2/bin/Debug/Gallery/DSC_0022.JPG";
            Picture picture = new Picture(path);
            EXIFMetaData meta = new EXIFMetaData();
            meta.Model = "DS3200";
            Assert.AreEqual("DS3200", meta.Model);
            picture.EXIFMetaData = meta;
            Assert.AreEqual(meta, picture.EXIFMetaData);
        }

        [TestMethod]
        public void TestPicturEXIFMetaData()
        {
            string path = "E:/Google Drive/FH/4 Semester/SWE/Uebung2/SWE_2/bin/Debug/Gallery/DSC_0022.JPG";
            Picture picture = new Picture(path);
            IPTCMetaData meta = new IPTCMetaData();
            meta.Title = "Test";
            picture.IPTCMetaData = meta;
            Assert.AreEqual(meta, picture.IPTCMetaData);
        }

        [TestMethod]
        public void TestPicturePhotographer()
        {
            string path = "E:/Google Drive/FH/4 Semester/SWE/Uebung2/SWE_2/bin/Debug/Gallery/DSC_0022.JPG";
            Picture picture = new Picture(path);
            Photographer photographer = new Photographer(1, "Yavor", "Ivanov", DateTime.Now, "test@emai.com", DateTime.Now);
            List<Photographer> phList = new List<Photographer>();
            phList.Add(photographer);
            picture.PhotograperList = phList;
            Assert.AreEqual(photographer, picture.PhotograperList[0]);
        }

        [TestMethod]
        public void TestPhotographerCreate()
        {
            Photographer photographer = new Photographer(1, "Yavor", "Ivanov", DateTime.Now, "test@emai.com", DateTime.Now);
            Assert.IsNotNull(photographer);
        }

        [TestMethod]
        public void TestPhotographerFirstName()
        {
            Photographer photographer = new Photographer(1, "Yavor", "Ivanov", DateTime.Now, "test@emai.com", DateTime.Now);
            Assert.AreEqual(photographer.FirstName, "Yavor");
        }

        [TestMethod]
        public void TestPhotographerLastName()
        {
            Photographer photographer = new Photographer(1, "Yavor", "Ivanov", DateTime.Now, "test@emai.com", DateTime.Now);
            Assert.AreEqual(photographer.LastName, "Ivanov");
        }

        [TestMethod]
        public void SQLConnectionOpenConnectionTest()
        {
            _sqlConn.Connection = connection;
            _sqlConn.openConnection();
        }

        [TestMethod]
        public void SQLConnectionCloseConnectionTest()
        {
            _sqlConn.Connection = connection;
            _sqlConn.openConnection();
            _sqlConn.closeConnection();
        }

        [TestMethod]
        public void SQLConnectionSelectAllImagesTest()
        {
            _sqlConn.Connection = connection;
            _sqlConn.openConnection();
            List<Picture> list = _sqlConn.SelectAllPictures();
            Assert.IsNotNull(list);
        }

        [TestMethod]
        public void SQLConnectionSelectAllPhotographersTest()
        {
            _sqlConn.Connection = connection;
            _sqlConn.openConnection();
            List<Photographer> list = _sqlConn.SelectAllPhotographers();
            Assert.IsNotNull(list);
        }

        [TestMethod]
        public void SQLConnectionSelectOnePhotographersTest()
        {
            _sqlConn.Connection = connection;
            _sqlConn.openConnection();
            Photographer ph = _sqlConn.SelectPhotograph(1);
            Assert.IsNotNull(ph);
        }

        [TestMethod]
        public void SQLConnectionSelectOnePictureTest()
        {
            _sqlConn.Connection = connection;
            _sqlConn.openConnection();
            Picture pic = _sqlConn.SelectPicture(1);
            Assert.IsNull(pic);
        }

        [TestMethod]
        public void PictureViewModelReloadPictureTest()
        {
            string path = "E:/Google Drive/FH/4 Semester/SWE/Uebung2/SWE_2/bin/Debug/Gallery/DSC_0022.JPG";
            Picture picture = new Picture(path);
            ObservableCollection<Picture> list = new ObservableCollection<Picture>();
            list.Add(picture);
            _picViewModel.PictureList = list;
            _picViewModel.reloadPicture(picture);
            Assert.AreEqual(picture.Path, _picViewModel.CurrentPicture.Path);
        }

        [TestMethod]
        public void PictureViewModelAllPictureTest()
        {
            List<Picture> list = _sqlConn.SelectAllPictures();
            _picViewModel.PictureList = new ObservableCollection<Picture>(list);
            Assert.AreNotEqual(0,_picViewModel.PictureList.Count);
        }

        [TestMethod]
        public void PhotographerViewModelTest()
        {
            Assert.AreNotEqual(0, _pViewModel.PhotographerList.Count);
        }
    }
}
