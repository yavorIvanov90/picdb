﻿using System;
using System.ComponentModel;
namespace SWE_2.Model
{
    public class Photographer : INotifyPropertyChanged
    {
        private int id = 0;
        private string firstname;
        private string lastname;
        private string email;
        private DateTime birthday;
        private DateTime regDate;
        private string note;
        private bool isSelected;
        public event PropertyChangedEventHandler PropertyChanged;


        public int Id
        {
            get { return id; }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }
        public string FirstName
        {
            get { return firstname; }
            set
            {
                firstname = value;
                OnPropertyChanged("Firstname");
            }
        }

        public string LastName
        {
            get { return lastname; }
            set
            {
                lastname = value;
                OnPropertyChanged("Lastname");
            }
        }
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged("Email");
            }
        }

        public DateTime Birthday
        {
            get { return birthday; }
            set
            {
                birthday = value;
                OnPropertyChanged("Birthday");
            }
        }

        public DateTime RegDate
        {
            get { return regDate; }
            set
            {
                regDate = value;
                OnPropertyChanged("RegDate");
            }
        }

        public string Note
        {
            get
            {
                return note;
            }
            set
            {
                note = value;
                OnPropertyChanged("Note");
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }




        public Photographer(int id, string firstname, string lastname, DateTime birthday, string email, DateTime regDay)
        {
            Id = id;
            FirstName = firstname;
            LastName = lastname;
            Birthday = birthday;
            Email = email;
            RegDate = regDay;
        }

        public Photographer(string firstname, string lastname, DateTime birthday, string email)
        {
            FirstName = firstname;
            LastName = lastname;
            Birthday = birthday;
            Email = email;
        }

        private void OnPropertyChanged(string propertyName)
        {

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
