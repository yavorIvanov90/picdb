﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using MetaData;
using System.Collections.ObjectModel;

namespace SWE_2.Model
{
    public class Picture : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public int Id { get; set; }
        public Image Image { get; private set; }
        public string Path { get; private set; }
        public Stream Stream { get; private set; }
        public List<int> PhotograperId { get; set; }
        public List<Photographer> PhotograperList { get; set; }
        public EXIFMetaData EXIFMetaData { get; set; }
        public IPTCMetaData IPTCMetaData { get; set; }
        private List<string> tags;

        //add iptc
        //add exif

        public Picture(string path)
        {
            PhotograperId = new List<int>();
            Tags = new List<string>();
            PhotograperList = new List<Photographer>();
            Path = path;
            Stream = new FileStream(Path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            EXIFMetaData = new EXIFMetaData();
            IPTCMetaData = new IPTCMetaData();
            CreateImage();
        }

        public Picture(Stream stream)
        {
            PhotograperId = new List<int>();
            Tags = new List<string>();
            PhotograperList = new List<Photographer>();
            Stream = stream;
            EXIFMetaData = new EXIFMetaData();
            IPTCMetaData = new IPTCMetaData();
            CreateImage();
        }
        public List<string> Tags
        {
            get
            { return tags; }
            set
            {
                tags = value; 
                OnPropertyChanged("Tags");
            }
        }
        private void CreateImage()
        {
            Stream.Seek(0, SeekOrigin.Begin);
            Image temp = new Image();
            temp.Source = BitmapFrame.Create(Stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            temp.VerticalAlignment = VerticalAlignment.Stretch;
            temp.Stretch = Stretch.Fill;
            Binding binding = new Binding("ActualHeight");
            binding.ElementName = "gallery_row";
            temp.SetBinding(RowDefinition.HeightProperty, binding);
            Image = temp;
        }

        public void LoadPictureFromPath()
        {
            Stream = new FileStream(Path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            CreateImage();
        }

        public void LoadPictureFromNewStream(Stream stream)
        {
            Stream = stream;
            CreateImage();
        }

        public void CreatePhotographerList(ObservableCollection<Photographer> photographers)
        {
            PhotograperList.Clear();
            foreach (Photographer ph in photographers)
            {
                if (PhotograperId.Contains(ph.Id))
                {
                    PhotograperList.Add(ph);
                }
            }
        }

        private void OnPropertyChanged(string propertyName)
        {

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }


}
