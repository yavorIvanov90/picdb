﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Org.BouncyCastle.Asn1.Mozilla;
using SWE_2.Model;

namespace SWE_2
{
    public class MySqlConnection
    {
        private MySql.Data.MySqlClient.MySqlConnection myConnection;
        private String connection;
        private string type = "MySql";
        private readonly ILogger<MySqlConnection> _logger;

        /// <summary>
        /// MSqlConnection constructor.
        /// </summary>
        public MySqlConnection(ILogger<MySqlConnection> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Opens new SQL connection with credentials.
        /// </summary>
        public void openConnection()
        {
            try
            {
                myConnection = new MySql.Data.MySqlClient.MySqlConnection(connection);
                myConnection.Open();
                _logger.LogDebug("MySQL Connection is opened!");
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Connection credentials.
        /// </summary>
        public String Connection
        {
            set
            {
                connection = value;
            }
        }

        private MySql.Data.MySqlClient.MySqlConnection MyConnection
        {
            set
            {
                myConnection = value;
            }
        }

        public string Type
        {
            get => type;
        }

        /// <summary>
        /// Closes the SQL connection.
        /// </summary>
        public void closeConnection()
        {
            try
            {
                myConnection.Close();
                _logger.LogDebug("MySQL Connection is closed!");
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Inserts new Image.
        /// </summary>
        public void insertImage(string path)
        {
            try
            {
                var sqlInsert = "INSERT INTO Image (path) " + "SELECT * FROM (SELECT @path) AS tmp " + "WHERE NOT EXISTS (SELECT path from Image where path = @path) LIMIT 1";
                var cmd = new MySqlCommand(sqlInsert, myConnection);
                cmd.Parameters.AddWithValue("@path", path);
                cmd.Prepare();
                int rows = cmd.ExecuteNonQuery();
                _logger.LogDebug($"Insert: {rows} rows affected");
                if (rows == 1)
                {
                    int id = 0;
                    var sqlSelect = "SELECT LAST_INSERT_ID()";
                    cmd = new MySqlCommand(sqlSelect, myConnection);
                    MySqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        id = int.Parse(rdr[0].ToString());
                    }
                    rdr.Close();
                    sqlInsert = "INSERT INTO EXIF (manufacturer,model,software,flash,color_space,image_id) values ('CANON','DS3200','2.3.1','off','sRGB',@image_id)";
                    cmd = new MySqlCommand(sqlInsert, myConnection);
                    cmd.Parameters.AddWithValue("@image_id", id);
                    cmd.Prepare();
                    rows = cmd.ExecuteNonQuery();
                    _logger.LogDebug($"Insert: {rows} rows affected");
                    sqlInsert = "INSERT INTO IPTC (title,description,city,image_id) values ('Title',' ','Wien',@image_id)";
                    cmd = new MySqlCommand(sqlInsert, myConnection);
                    cmd.Parameters.AddWithValue("@image_id", id);
                    cmd.Prepare();
                    rows = cmd.ExecuteNonQuery();
                    _logger.LogDebug($"Insert: {rows} rows affected");
                }

            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Inserts new photograph.
        /// </summary>
        public void insertPhotograph(String firstname, String lastname, string birthday, string email)
        {
            try
            {
                var sqlInsert = "INSERT INTO Photograph (firstname,lastname,birthday_date,email) " +
                    "SELECT * FROM (SELECT @firstname, @lastname, @birthday,@email) AS tmp " +
                    "WHERE NOT EXISTS ( SELECT firstname,lastname,email from Photograph where firstname = @firstname and lastname = @lastname and email = @email) LIMIT 1";
                var cmd = new MySqlCommand(sqlInsert, myConnection);
                cmd.Parameters.AddWithValue("@firstname", firstname);
                cmd.Parameters.AddWithValue("@lastname", lastname);
                cmd.Parameters.AddWithValue("@birthday", birthday);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Prepare();
                int rows = cmd.ExecuteNonQuery();
                _logger.LogDebug($"Insert: {rows} rows affected");
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Inserts new photograph.
        /// </summary>
        public void insertPhotograph(Photographer ph)
        {
            try
            {
                var sqlInsert = "INSERT INTO Photograph (firstname,lastname,birthday_date,email) " +
                    "SELECT * FROM (SELECT @firstname, @lastname, @birthday,@email) AS tmp " +
                    "WHERE NOT EXISTS ( SELECT firstname,lastname,email from Photograph where firstname = @firstname and lastname = @lastname and email = @email) LIMIT 1";
                var cmd = new MySqlCommand(sqlInsert, myConnection);
                cmd.Parameters.AddWithValue("@firstname", ph.FirstName);
                cmd.Parameters.AddWithValue("@lastname", ph.LastName);
                cmd.Parameters.AddWithValue("@birthday", ph.Birthday);
                cmd.Parameters.AddWithValue("@email", ph.Email);
                cmd.Prepare();
                int rows = cmd.ExecuteNonQuery();
                _logger.LogDebug($"Insert: {rows} rows affected");
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Deletes an Image.
        /// </summary>
        public void deleteImage(string image_path)
        {
            try
            {
                var sqlDelete = "DELETE FROM Image where image_path = @image_path";
                var cmd = new MySqlCommand(sqlDelete, myConnection);
                cmd.Parameters.AddWithValue("@image_path", image_path);
                cmd.Prepare();
                int rows = cmd.ExecuteNonQuery();
                _logger.LogDebug($"Delete: {rows} rows affected");
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        public void deleteImage(Picture pic)
        {
            try
            {
                var sqlDelete = "DELETE FROM Image where id = @image_id";
                var cmd = new MySqlCommand(sqlDelete, myConnection);
                cmd.Parameters.AddWithValue("@image_id", pic.Id);
                cmd.Prepare();
                int rows = cmd.ExecuteNonQuery();

                sqlDelete = "DELETE FROM EXIF where image_id = @image_id";
                cmd = new MySqlCommand(sqlDelete, myConnection);
                cmd.Parameters.AddWithValue("@image_id", pic.Id);
                cmd.Prepare();
                rows = cmd.ExecuteNonQuery();

                sqlDelete = "DELETE FROM IPTC where image_id = @image_id";
                cmd = new MySqlCommand(sqlDelete, myConnection);
                cmd.Parameters.AddWithValue("@image_id", pic.Id);
                cmd.Prepare();
                rows = cmd.ExecuteNonQuery();

                sqlDelete = "DELETE FROM Photograph_Image where image_id = @image_id";
                cmd = new MySqlCommand(sqlDelete, myConnection);
                cmd.Parameters.AddWithValue("@image_id", pic.Id);
                cmd.Prepare();
                rows = cmd.ExecuteNonQuery();

                _logger.LogDebug($"Delete: {rows} rows affected");
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Deletes a photograph.
        /// </summary>
        public void deletePhotograph(int photograph_id)
        {
            try
            {
                var sqlDelete = "DELETE FROM Photograph where id = @photograph_id";
                var cmd = new MySqlCommand(sqlDelete, myConnection);
                cmd.Parameters.AddWithValue("@photograph_id", photograph_id);
                cmd.Prepare();
                int rows = cmd.ExecuteNonQuery();
                _logger.LogDebug($"Delete: {rows} rows affected");
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }
        public List<Picture> SelectAllPictures()
        {
            List<Picture> images = new List<Picture>();
            try
            {
                //var sqlSelect = "SELECT id,path,tags FROM Image";
                //var sqlSelect = "SELECT pic.id,ph.image_id FROM Image pic join Photograph_Image ph on pic.id = ph.image_id";
                var sqlSelect = "SELECT i.id,i.path,i.tags,e.manufacturer,e.model,e.software,e.flash,e.color_space,c.title,c.description,c.city FROM Image i JOIN EXIF e on i.id = e.image_id JOIN IPTC c on i.id = c.image_id";
                var cmd = new MySqlCommand(sqlSelect, myConnection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Picture pic = new Picture(rdr[1] as string);
                    pic.Id = (int)rdr.GetUInt32(0);
                    if ((rdr[2] as string) != null)
                    {
                        if ((rdr[2] as string) != "")
                            pic.Tags = new List<string>((rdr[2] as string).Split(','));
                    }
                    pic.EXIFMetaData.Manufacturer = (rdr[3] as string);
                    pic.EXIFMetaData.Model = (rdr[4] as string);
                    pic.EXIFMetaData.Software = (rdr[5] as string);
                    pic.EXIFMetaData.Flash = (rdr[6] as string);
                    pic.EXIFMetaData.ColorSpace = (rdr[7] as string);
                    pic.IPTCMetaData.Title = (rdr[8] as string);
                    pic.IPTCMetaData.Description = (rdr[9] as string);
                    pic.IPTCMetaData.City = (rdr[10] as string);
                    images.Add(pic);
                    _logger.LogDebug($"Picture {pic.Path} created!");

                }
                rdr.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
            foreach (Picture pic in images)
            {
                pic.PhotograperId = JoinPhotographerToPicture(pic.Id);
            }
            return images;
        }

        public List<Photographer> SelectAllPhotographers()
        {
            List<Photographer> photohraphers = new List<Photographer>();
            try
            {
                var sqlSelect = "SELECT id,firstname, lastname, birthday_date, email, reg_date FROM Photograph";
                var cmd = new MySqlCommand(sqlSelect, myConnection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    int id = int.Parse(rdr[0].ToString());
                    string firstname = rdr[1].ToString();
                    string lastname = rdr[2].ToString();
                    DateTime birthday = DateTime.Parse(rdr[3].ToString());
                    string email = rdr[4].ToString();
                    DateTime reg = DateTime.Parse(rdr[5].ToString().Split(' ')[0]);
                    Photographer photohrapher = new Photographer(id, firstname, lastname, birthday, email, reg);
                    photohraphers.Add(photohrapher);
                }
                rdr.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
            _logger.LogDebug($"{photohraphers.Count} Photohraphers selected!");
            return photohraphers;
        }

        public Photographer SelectPhotograph(int photograph_id)
        {
            Photographer photohrapher = null;
            try
            {
                var sqlSelect = "SELECT id,firstname,lastname,birthday_date,email,reg_date FROM Photograph where id = @photograph_id";
                var cmd = new MySqlCommand(sqlSelect, myConnection);
                cmd.Parameters.AddWithValue("@photograph_id", photograph_id);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    int id = int.Parse(rdr[0].ToString());
                    string firstname = rdr[1].ToString();
                    string lastname = rdr[2].ToString();
                    DateTime birthday = DateTime.Parse(rdr[3].ToString());
                    string email = rdr[4].ToString();
                    DateTime reg = DateTime.Parse(rdr[5].ToString().Split(' ')[0]);
                    photohrapher = new Photographer(id, firstname, lastname, birthday, email, reg);
                }
                rdr.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
            _logger.LogDebug($"{photohrapher.FirstName} Photohrapher selected!");
            return photohrapher;
        }

        public Picture SelectPicture(int pic_id)
        {
            Picture pic = null;

            return pic;
        }

        public void UpdatePhotographer(Photographer ph)
        {
            var sqlUpdate = "UPDATE Photograph SET firstname=@firstname, lastname=@lastname, birthday_date=@birthday,email=@email where id = @photograph_id";
            var cmd = new MySqlCommand(sqlUpdate, myConnection);
            cmd.Parameters.AddWithValue("@firstname", ph.FirstName);
            cmd.Parameters.AddWithValue("@lastname", ph.LastName);
            cmd.Parameters.AddWithValue("@birthday", ph.Birthday);
            cmd.Parameters.AddWithValue("@email", ph.Email);
            cmd.Parameters.AddWithValue("@photograph_id", ph.Id);
            cmd.Prepare();
            int rows = cmd.ExecuteNonQuery();
            _logger.LogDebug($"Update: {rows} rows affected");
        }

        public void UpdateImage(Picture pic)
        {
            var sqlUpdate = "UPDATE Image SET path=@path, tags=@tags where id = @pic_id";
            var cmd = new MySqlCommand(sqlUpdate, myConnection);
            string tags = "";
            if (pic.Tags != null)
            {
                tags = String.Join(",", pic.Tags.ToArray());
            }
            cmd.Parameters.AddWithValue("@path", pic.Path);
            cmd.Parameters.AddWithValue("@tags", tags);
            cmd.Parameters.AddWithValue("@pic_id", pic.Id);
            cmd.Prepare();
            int rows = cmd.ExecuteNonQuery();
            _logger.LogDebug($"Update: {rows} rows affected");
        }

        public void UpdateEXIFImage(Picture pic)
        {
            var sqlUpdate = "UPDATE EXIF SET manufacturer=@manufacturer,model=@model,software=@software,flash=@flash,color_space=@color_space where id = @pic_id";
            var cmd = new MySqlCommand(sqlUpdate, myConnection);
            cmd.Parameters.AddWithValue("@manufacturer", pic.EXIFMetaData.Manufacturer);
            cmd.Parameters.AddWithValue("@model", pic.EXIFMetaData.Model);
            cmd.Parameters.AddWithValue("@software", pic.EXIFMetaData.Software);
            cmd.Parameters.AddWithValue("@flash", pic.EXIFMetaData.Flash);
            cmd.Parameters.AddWithValue("@color_space", pic.EXIFMetaData.ColorSpace);
            cmd.Parameters.AddWithValue("@pic_id", pic.Id);
            cmd.Prepare();
            int rows = cmd.ExecuteNonQuery();
            _logger.LogDebug($"Update: {rows} rows affected");
        }

        public void UpdateIPTCImage(Picture pic)
        {
            var sqlUpdate = "UPDATE IPTC SET title=@title,description=@description,city=@city where id = @pic_id";
            var cmd = new MySqlCommand(sqlUpdate, myConnection);
            cmd.Parameters.AddWithValue("@title", pic.IPTCMetaData.Title);
            cmd.Parameters.AddWithValue("@description", pic.IPTCMetaData.Description);
            cmd.Parameters.AddWithValue("@city", pic.IPTCMetaData.City);
            cmd.Parameters.AddWithValue("@pic_id", pic.Id);
            cmd.Prepare();
            int rows = cmd.ExecuteNonQuery();
            _logger.LogDebug($"Update: {rows} rows affected");
        }

        public void UpdatePhotographerToPicture(List<Photographer> phs, Picture pic)
        {
            try
            {
                var sqlDelete = "DELETE FROM Photograph_Image where image_id = @pic_id";
                var cmd = new MySqlCommand(sqlDelete, myConnection);
                cmd.Parameters.AddWithValue("@pic_id", pic.Id);
                cmd.Prepare();
                int rows = cmd.ExecuteNonQuery();
                _logger.LogDebug($"Delete: {rows} rows affected");

                foreach (Photographer ph in phs)
                {
                    var sqlInsert = "INSERT INTO Photograph_Image(photograph_id,image_id) VALUES(@photograph_id,@image_id)";
                    cmd = new MySqlCommand(sqlInsert, myConnection);
                    cmd.Parameters.AddWithValue("@photograph_id", ph.Id);
                    cmd.Parameters.AddWithValue("@image_id", pic.Id);
                    cmd.Prepare();
                    rows = cmd.ExecuteNonQuery();
                    _logger.LogDebug($"Insert: {rows} rows affected");
                }
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        public List<int> JoinPhotographerToPicture(int pic_id)
        {
            List<int> photographers = new List<int>();
            try
            {
                var sqlSelect = "SELECT ph.photograph_id,ph.image_id FROM Image pic join Photograph_Image ph on pic.id = ph.image_id WHERE pic.id = @id";
                var cmd = new MySqlCommand(sqlSelect, myConnection);
                cmd.Parameters.AddWithValue("@id", pic_id);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    int id = (int)rdr.GetUInt32(0);
                    photographers.Add(id);
                }
                rdr.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
            return photographers;
        }



        public void createPhotographers()
        {
            insertPhotograph("Yavor", "Ivanov", "1990-02-04", "if18b046@technikum-wien.at");
            insertPhotograph("Robert", "Capa", "1913-10-22", "robert.capa@gmail.com");
            insertPhotograph("Ansel", "Adams", "1902-02-20", "ansel_adams@gmail.com");
        }

    }
}
