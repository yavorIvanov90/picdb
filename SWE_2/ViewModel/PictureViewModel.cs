﻿using SWE_2.Command;
using SWE_2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SWE_2.ViewModel
{
    public class PictureViewModel : INotifyPropertyChanged
    {
        private MySqlConnection _sqlConn;
        private Picture currentPicture;
        private ObservableCollection<Photographer> currentPicturePhotographers;
        private ObservableCollection<Picture> pictureList;
        private ObservableCollection<Picture> currentPictureList;
        private readonly PhotographerViewModel _phViewModel;
        private ObservableCollection<Image> pictureListAsImage;

        public ObservableCollection<Image> PictureListAsImage
        {
            get
            {
                return pictureListAsImage;
            }
            set
            {
                pictureListAsImage = value;
                OnPropertyChanged("PictureListAsImage");
            }
        }

        public ObservableCollection<Photographer> CurrentPicturePhotographers
        {
            get
            {
                return currentPicturePhotographers;
            }
            set
            {
                currentPicturePhotographers = value;
                OnPropertyChanged("CurrentPicturePhotographers");
            }
        }

        public Picture CurrentPicture
        {
            get { return currentPicture; }
            private set
            {
                currentPicture = value;
                MarkPhotographers();
                OnPropertyChanged("CurrentPicture");
            }
        }

        public void MarkPhotographers()
        {
            ObservableCollection<Photographer> temp = _phViewModel.PhotographerList;
            //PhotgrapherGrid.ItemsSource = ViewModel._pViewModel.PhotographerList;
            foreach (Photographer ph in temp)
            {

                ph.IsSelected = false;
                if (currentPicture.PhotograperId.Contains(ph.Id))
                {
                    ph.IsSelected = true;
                }
            }
            CurrentPicturePhotographers = temp;
        }

        public ObservableCollection<Image> PicturesToImages(ObservableCollection<Picture> pictures)
        {
            ObservableCollection<Image> temp = new ObservableCollection<Image>();
            foreach (Picture pic in pictures)
            {
                temp.Add(pic.Image);
            }
            return temp;
        }

        public ObservableCollection<Picture> PictureList
        {
            get
            {
                return pictureList;
            }
            set
            {
                pictureList = value;
                PictureListAsImage = PicturesToImages(pictureList);
                CurrentPictureList = pictureList;
                if (pictureList.Count > 0)
                    SetCurrentPicture(pictureList[0]);
                OnPropertyChanged("PictureList");
            }
        }

        public ObservableCollection<Picture> CurrentPictureList
        {
            get
            {
                return currentPictureList;
            }
            set
            {
                currentPictureList = value;
                PictureListAsImage = PicturesToImages(currentPictureList);
                if (currentPictureList.Count > 0)
                    SetCurrentPicture(currentPictureList[0]);
                OnPropertyChanged("CurrentPictureList");
            }
        }
        public RelayCommand SavePicture { get; private set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public PictureViewModel(MySqlConnection sqlConn, PhotographerViewModel phViewModel)
        {
            _sqlConn = sqlConn;
            _phViewModel = phViewModel;
            PictureList = new ObservableCollection<Picture>();
            SavePicture = new RelayCommand(Update, CanSave);
        }

        public void AfterCall()
        {
            PictureList = new ObservableCollection<Picture>(_sqlConn.SelectAllPictures());
            SetCurrentPicture(PictureList[0]);
        }
        public void SetCurrentPicture(Picture pic)
        {
            if (pic != null)
            {
                CurrentPicture = pic;
                currentPicture.CreatePhotographerList(_phViewModel.PhotographerList);
                ReadMetaData(pic);
            }
        }

        public void Update(object s)
        {
            /*Picture pic = CurrentPicture;
            var jpgStream = pic.Stream;
            jpgStream.Position = 0;
            JpegBitmapDecoder jpgDecoder = new JpegBitmapDecoder(jpgStream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            BitmapFrame jpgFrame = jpgDecoder.Frames[0];
            InPlaceBitmapMetadataWriter jpgInplace = jpgFrame.CreateInPlaceBitmapMetadataWriter();
            if (jpgInplace.TrySave())
            {
                jpgInplace.SetQuery("/Text/Description", "Have a nice day.");
            }
            Console.WriteLine("Save Picture");*/
            //var query = jpgInplace.GetQuery("/Text/Description");
            //Console.WriteLine("Read meta data: " + query);
            //pic.Stream.Close();
            //reloadPicture(pic);
            UpdateTagsInDB();
            UpdateEXIFInDB();
            UpdateIPTCInDB();
        }

        public bool CanSave(object s)
        {
            return true;
        }

        public void ReadMetaData(Picture pic)
        {
            var jpgStream = pic.Stream;
            jpgStream.Position = 0;
            JpegBitmapDecoder jpgDecoder = new JpegBitmapDecoder(jpgStream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            BitmapFrame jpgFrame = jpgDecoder.Frames[0];
            InPlaceBitmapMetadataWriter jpgInplace = jpgFrame.CreateInPlaceBitmapMetadataWriter();
            var query = jpgInplace.GetQuery("/Text/Description");
            Console.WriteLine("Read meta data: " + query);
        }

        public void reloadPicture(Picture pic)
        {
            var index = PictureList.IndexOf(pic);
            var id = pic.Id;
            var path = pic.Path;
            //PictureList.Remove(pic);
            Picture newPic = new Picture(path);
            newPic.Id = id;
            _sqlConn.JoinPhotographerToPicture(id);
            // PictureList.Insert(index, newPic);
            PictureList[index] = newPic;
            SetCurrentPicture(newPic);
        }

        public void UpdatePhotographerId()
        {
            currentPicture.PhotograperId = _sqlConn.JoinPhotographerToPicture(currentPicture.Id);
            MarkPhotographers();
        }

        private void UpdateTagsInDB()
        {
            _sqlConn.UpdateImage(currentPicture);
        }

        private void UpdateEXIFInDB()
        {
            _sqlConn.UpdateEXIFImage(currentPicture);
        }

        private void UpdateIPTCInDB()
        {
            _sqlConn.UpdateIPTCImage(currentPicture);
        }

        // Create the OnPropertyChanged method to raise the event
        // The calling member's name will be used as the parameter.
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
