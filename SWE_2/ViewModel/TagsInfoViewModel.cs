﻿using SWE_2.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SWE_2.ViewModel
{
    public class TagsInfoViewModel : INotifyPropertyChanged
    {
        private readonly PictureViewModel _picViewModel;
        private Dictionary<String, int> tagsList;
        public event PropertyChangedEventHandler PropertyChanged;
        public TagsInfoViewModel(PictureViewModel picViewModel)
        {
            _picViewModel = picViewModel;
            TagsList = new Dictionary<String, int>();
        }

        public Dictionary<String, int> TagsList
        {
            get { return tagsList; }
            set
            {
                tagsList = value;
                OnPropertyChanged("TagsList");
            }
        }

        public void CreateTagsList()
        {
            Dictionary<String, int> temp = new Dictionary<String, int>();
            foreach (Picture pic in _picViewModel.PictureList)
            {
                foreach (String st in pic.Tags)
                {
                    if (temp.ContainsKey(st))
                    {
                        temp[st] += 1;
                    }
                    else
                    {
                        temp.Add(st, 1);
                    }
                }
            }
            var sortedDict = temp.OrderByDescending(x => x.Value);
            TagsList = sortedDict.ToDictionary(pair => pair.Key, pair => pair.Value);
        }
        // Create the OnPropertyChanged method to raise the event
        // The calling member's name will be used as the parameter.
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
