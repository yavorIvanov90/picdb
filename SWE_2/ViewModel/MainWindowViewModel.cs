﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Win32;
using SWE_2.Command;
using SWE_2.Model;
using SWE_2.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace SWE_2.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public int ImageIndex { get; set; }
        private string text;
        private string descriptionText;
        private int tabControlSelectedIndex;

        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }
        public string DescriptionBox
        {
            get { return descriptionText; }
            set
            {
                descriptionText = value;
                OnPropertyChanged("DescriptionBox");
            }
        }

        public int TabControlSelectedIndex
        {
            get
            {
                return tabControlSelectedIndex;
            }
            set
            {
                tabControlSelectedIndex = value;
                ChangeControlTabText();
                OnPropertyChanged("TabControlSelectedIndex");
            }
        }

        public List<Image> Gallery { get; set; }
        public List<ComboBoxItem> ExifComboBoxItems { get; set; }
        public List<ComboBoxItem> IptcComboBoxItems { get; set; }
        private string[] Images { get; set; }
        private readonly ILogger _logger;
        private readonly IConfiguration _conf;
        private MySqlConnection _sqlConn;
        private PictureViewModel _picViewModel;
        public readonly PhotographerView _pView;
        public readonly PhotographerViewModel _pViewModel;
        public readonly ImageInfoView _imgViewInfo;
        public readonly TagsInfoView _tagsViewInfo;
        public readonly TagsInfoViewModel _tagsInfoViewModel;

        public RelayCommand SelectedItemChangedCommand { get; private set; }

        public Picture CurrentPicture { get; private set; }

        public MetaData.EXIFMetaData exif = new MetaData.EXIFMetaData();

        public event PropertyChangedEventHandler PropertyChanged;
        public ItemCollection PicPhotographers { get; set; }

        public MainWindowViewModel(ILogger<MainWindowViewModel> logger,
            IConfiguration conf,
            MySqlConnection sqlConn,
            PictureViewModel picViewModel,
            PhotographerView pView,
            PhotographerViewModel pViewModel,
            ImageInfoView imgInfoViewModel,
            TagsInfoView tagsViewInfo,
            TagsInfoViewModel tagsInfoViewModel)
        {
            _logger = logger;
            _conf = conf;
            MySql = sqlConn;
            _picViewModel = picViewModel;
            _pView = pView;
            _imgViewInfo = imgInfoViewModel;
            _tagsViewInfo = tagsViewInfo;
            _tagsInfoViewModel = tagsInfoViewModel;
            _pViewModel = pViewModel;

            SelectedItemChangedCommand = new RelayCommand(Update, CanSave);

            ComboBoxItem cie1 = new ComboBoxItem();
            ComboBoxItem cie2 = new ComboBoxItem();
            ComboBoxItem cie3 = new ComboBoxItem();
            ComboBoxItem cie4 = new ComboBoxItem();
            ComboBoxItem cie5 = new ComboBoxItem();
            ComboBoxItem cie6 = new ComboBoxItem();


            cie1.Content = "Manufactor";
            cie2.Content = "Model";
            cie3.Content = "Software";
            cie4.Content = "Flash";
            cie5.Content = "Color space";

            ExifComboBoxItems = new List<ComboBoxItem>();
            ExifComboBoxItems.Add(cie1);
            ExifComboBoxItems.Add(cie2);
            ExifComboBoxItems.Add(cie3);
            ExifComboBoxItems.Add(cie4);
            ExifComboBoxItems.Add(cie5);
            ExifComboBoxItems.Add(cie6);

            ComboBoxItem cii1 = new ComboBoxItem();
            ComboBoxItem cii2 = new ComboBoxItem();
            ComboBoxItem cii3 = new ComboBoxItem();

            cii1.Content = "Title";
            cii2.Content = "Description";
            cii3.Content = "City";

            IptcComboBoxItems = new List<ComboBoxItem>();
            IptcComboBoxItems.Add(cii1);
            IptcComboBoxItems.Add(cii2);
            IptcComboBoxItems.Add(cii3);

            Text = "";
            DescriptionBox = "";
            _logger.LogDebug("MainWindowViewModel created");

            Images = Directory.GetFiles(System.IO.Path.Combine(_conf.GetValue<String>("Path")), "*.jpg");
            //ImageIndex = 0;

            _sqlConn.createPhotographers();
            PicViewModel.AfterCall();

            _tagsInfoViewModel.CreateTagsList();
            LoadImages();
        }

        public PictureViewModel PicViewModel
        {
            get
            { return _picViewModel; }
            set
            {
                _picViewModel = value;
                OnPropertyChanged("PicViewModel");
            }
        }

        public MySqlConnection MySql
        {
            get
            { return _sqlConn; }
            set
            {
                _sqlConn = value;
                OnPropertyChanged("MySql");
            }
        }

        public void LoadImages()
        {
            var gallery_path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Gallery/");
            string[] paths = Directory.GetFiles(gallery_path, "*.jpg");
            foreach (string image in paths)
                _sqlConn.insertImage(image);
        }

        public void AddNewPicture()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.JPG)|*.JPG;";
            dialog.CheckFileExists = true;
            dialog.Multiselect = false;
            var result = dialog.ShowDialog();
            if (result == false)
            {
                return;
            }

            Picture p = new Picture(dialog.FileName);
            MySql.insertImage(p.Path);
            ObservableCollection<Picture> temp = PicViewModel.PictureList;
            temp.Add(p);
            PicViewModel.PictureList = temp;
            //picture_list_box.PictureListBoxTemplate.ItemsSource = _picViewModel.PictureListAsImage;
        }

        public void DeletePicture()
        {
            ObservableCollection<Picture> temp = PicViewModel.CurrentPictureList;
            temp.Remove(PicViewModel.CurrentPicture);
            MySql.deleteImage(PicViewModel.CurrentPicture);
            PicViewModel.CurrentPictureList = temp;
            // picture_list_box.PictureListBoxTemplate.ItemsSource = _picViewModel.PictureListAsImage;
        }

        public void imageChanged()
        {
            _logger.LogInformation($"Main image changed");
            //ImageIndex = picture_list_box.PictureListBoxTemplate.SelectedIndex;
            bool success = false;
            try
            {
                //MarkPicturePhotographers();
                //tab_control.SelectedIndex = 0;
                //IPTCcomboBox.SelectedIndex = 0;
                DescriptionBox = (String.Join(",", PicViewModel.CurrentPicture.Tags.ToArray()));
                Text = PicViewModel.CurrentPicture.IPTCMetaData.Title;
                success = true;
            }
            catch (Exception e)
            {
                _logger.LogDebug(e.ToString());
            }
            finally
            {
                if (!success)
                {
                    // picture_list_box.PictureListBoxTemplate.SelectedIndex = 0;
                    // MarkPicturePhotographers();
                    // tab_control.SelectedIndex = 0;
                    //IPTCcomboBox.SelectedIndex = 0;
                    DescriptionBox = (String.Join(",", PicViewModel.CurrentPicture.Tags.ToArray()));
                    Text = PicViewModel.CurrentPicture.IPTCMetaData.Title;
                }
            }
        }

        // Create the OnPropertyChanged method to raise the event
        // The calling member's name will be used as the parameter.
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void Update(object s)
        {
            ;
            int index = (int)s;
            if (PicViewModel.CurrentPictureList.Count > 0)
            {
                if (index < 0)
                    index = 0;
                PicViewModel.SetCurrentPicture(PicViewModel.CurrentPictureList[index]);
                TabControlSelectedIndex = 0;
                imageChanged();
            }
        }

        public bool CanSave(object s)
        {
            return true;
        }

        private void ChangeControlTabText()
        {
            if (TabControlSelectedIndex == 0)
            {
                Text = PicViewModel.CurrentPicture.IPTCMetaData.Title;
            }
            else if (TabControlSelectedIndex == 1)
            {
                Text = PicViewModel.CurrentPicture.EXIFMetaData.Manufacturer;
            }
        }
    }
}
