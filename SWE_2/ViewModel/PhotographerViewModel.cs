﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Data;
using System.Windows.Input;
using SWE_2.Model;
using SWE_2.Command;
using System.Collections.ObjectModel;
using Microsoft.Extensions.Logging;
using System.Windows;
using System.Windows.Media;
using System.Runtime.CompilerServices;

namespace SWE_2.ViewModel
{
    public class PhotographerViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private MySqlConnection _sqlConn;
        private readonly ILogger<PhotographerViewModel> _logger;
        private ObservableCollection<Photographer> photographerList;

        public ObservableCollection<Photographer> PhotographerList
        {
            get
            {
                return photographerList;
            }
            set
            {
                OnPropertyChanged("PhotographerList");
                photographerList = value;
            }
        }
        public RelayCommand SavePhotographer { get; private set; }

        public PhotographerViewModel(MySqlConnection sqlConn, ILogger<PhotographerViewModel> logger)
        {
            _sqlConn = sqlConn;
            _logger = logger;
            UpdatePhotographersList();
            SavePhotographer = new RelayCommand(Update, CanSave);
        }


        public void AddOrUpdate(Photographer ph)
        {
            if (ph.LastName == null || ph.LastName == "")
            {
                MessageBox.Show("Photographer's last name cannot be empty!");
                return;
            }
            if (ph.Birthday >= DateTime.Now)
            {
                MessageBox.Show($"Photographer's birthday cannot be empty or greater than { DateTime.Now}!");
                return;
            }
            if (ph.Id == 0)
            {
                _sqlConn.insertPhotograph(ph);
                _logger.LogDebug($"Photographer {ph.FirstName} added!");
            }
            else
            {
                _sqlConn.UpdatePhotographer(ph);
                _logger.LogDebug($"Photographer {ph.FirstName} updated!");
            }
            UpdatePhotographersList();
        }

        public void DeletePhotographer(int id)
        {
            _sqlConn.deletePhotograph(id);
            UpdatePhotographersList();
            _logger.LogDebug($"Photographer with id {id} deleted!");
        }

        private void UpdatePhotographersList()
        {
            PhotographerList = new ObservableCollection<Photographer>(_sqlConn.SelectAllPhotographers());
        }

        public void Update(object s)
        {
            Photographer ph = s as Photographer;
            AddOrUpdate(ph);

        }

        public bool CanSave(object s)
        {
            return true;
        }

        // Create the OnPropertyChanged method to raise the event
        // The calling member's name will be used as the parameter.
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

}
