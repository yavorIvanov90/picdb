﻿using SWE_2.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWE_2.View
{
    /// <summary>
    /// Interaction logic for TagsInfoView.xaml
    /// </summary>
    public partial class TagsInfoView : Window
    {
        private readonly TagsInfoViewModel _tagsInfoViewModel;
        public TagsInfoView(TagsInfoViewModel tagsInfoViewModel)
        {
            _tagsInfoViewModel = tagsInfoViewModel;
            DataContext = _tagsInfoViewModel;
            InitializeComponent();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            e.Cancel = true;
        }

        private void Print(object sender, RoutedEventArgs e)
        {
            PrintDialog printDlg = new PrintDialog();
            printDlg.PrintVisual(grid, "User Control Printing.");
        }
    }
}
