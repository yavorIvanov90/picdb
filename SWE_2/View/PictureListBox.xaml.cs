﻿using SWE_2.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWE_2.View
{
    /// <summary>
    /// Interaction logic for PictureListBox.xaml
    /// </summary>
    public partial class PictureListBox : UserControl
    {
        public PictureListBox()
        {
            InitializeComponent();
        }
    }
}
