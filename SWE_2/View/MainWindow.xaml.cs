﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using MetaData;
using System.ComponentModel;
using SWE_2.ViewModel;
using SWE_2.View;
using SWE_2.Model;
using System.Collections.ObjectModel;
using System.Security.Permissions;
using Microsoft.Win32;

namespace SWE_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _conf;
        private MainWindowViewModel _viewModel;

        public MainWindow(ILogger<MainWindow> logger, MainWindowViewModel viewModel, IConfiguration conf)
        {
            _logger = logger;
            _conf = conf;
            ViewModel = viewModel;
            Title = _conf.GetValue<String>("AppTitle");
            _logger.LogDebug("MainWindow Loaded");
            DataContext = ViewModel;
            InitializeComponent();
        }

        public MainWindowViewModel ViewModel
        {
            get { return _viewModel; }
            set { _viewModel = value; }
        }

        private void changeHeight(object sender, SizeChangedEventArgs args)
        {
            PictureListBox li = sender as PictureListBox;
            if (li != null)
            {
                foreach (Picture pic in ViewModel.PicViewModel.PictureList)
                {
                    if (li.ActualHeight - 50 > 0)
                        pic.Image.Height = li.ActualHeight - 50;
                }
            }
        }

        private void ExifComboBox_DropDownClosed(object sender, EventArgs e)
        {
            _logger.LogDebug("Change DropDown");
            ComboBox cmb = sender as ComboBox;
            switch (cmb.SelectedIndex)
            {
                case 0:
                    {
                        ViewModel.Text = ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.Manufacturer;
                        break;
                    }
                case 1:
                    {
                        ViewModel.Text = ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.Model;
                        break;
                    }
                case 2:
                    {
                        ViewModel.Text = ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.Software;
                        break;
                    }
                case 3:
                    {
                        ViewModel.Text = ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.Flash;
                        break;
                    }
                case 4:
                    {
                        ViewModel.Text = ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.ColorSpace;
                        break;
                    }
            }
        }

        private void IptcComboBox_DropDownClosed(object sender, EventArgs e)
        {
            _logger.LogDebug("Change DropDown");
            ComboBox cmb = sender as ComboBox;
            switch (cmb.SelectedIndex)
            {
                case 0:
                    {
                        ViewModel.Text = ViewModel.PicViewModel.CurrentPicture.IPTCMetaData.Title;
                        break;
                    }
                case 1:
                    {
                        ViewModel.Text = ViewModel.PicViewModel.CurrentPicture.IPTCMetaData.Description;
                        break;
                    }
                case 2:
                    {
                        ViewModel.Text = ViewModel.PicViewModel.CurrentPicture.IPTCMetaData.City;
                        break;
                    }
            }
        }

        private void SavePicture(object sender, RoutedEventArgs e)
        {
            _logger.LogDebug("Save Picture");
            if (_viewModel.DescriptionBox.Contains(","))
            {
                ViewModel.PicViewModel.CurrentPicture.Tags = new List<string>(_viewModel.DescriptionBox.Split(','));
            }
            else
            {
                if (_viewModel.DescriptionBox != "")
                {
                    ViewModel.PicViewModel.CurrentPicture.Tags = new List<string>();
                    ViewModel.PicViewModel.CurrentPicture.Tags.Add(_viewModel.DescriptionBox);
                }
            }
            if ((sender as Button).Name == "EXIFButton")
            {
                switch (EXIFcomboBox.SelectedIndex)
                {
                    case 0:
                        {
                            ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.Manufacturer = ViewModel.Text;
                            break;
                        }
                    case 1:
                        {
                            ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.Model = ViewModel.Text;
                            break;
                        }
                    case 2:
                        {
                            ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.Software = ViewModel.Text;
                            break;
                        }
                    case 3:
                        {
                            ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.Flash = _viewModel.Text;
                            break;
                        }
                    case 4:
                        {
                            ViewModel.PicViewModel.CurrentPicture.EXIFMetaData.ColorSpace = ViewModel.Text;
                            break;
                        }
                }
            }
            if ((sender as Button).Name == "IPTCButton")
            {
                switch (IPTCcomboBox.SelectedIndex)
                {
                    case 0:
                        {
                            ViewModel.PicViewModel.CurrentPicture.IPTCMetaData.Title = ViewModel.Text;
                            break;
                        }
                    case 1:
                        {
                            ViewModel.PicViewModel.CurrentPicture.IPTCMetaData.Description = ViewModel.Text;
                            break;
                        }
                    case 2:
                        {
                            ViewModel.PicViewModel.CurrentPicture.IPTCMetaData.City = ViewModel.Text;
                            break;
                        }
                }
            }
            _viewModel._tagsInfoViewModel.CreateTagsList();
        }

        private void ShowPhotographWindow(object sender, RoutedEventArgs e)
        {
            _viewModel._pView.Visibility = Visibility.Visible;
        }

        private void ShowImageInfoWindow(object sender, RoutedEventArgs e)
        {
            _viewModel._imgViewInfo.Visibility = Visibility.Visible;
        }

        private void ShowTagsInfoWindow(object sender, RoutedEventArgs e)
        {
            _viewModel._tagsViewInfo.Visibility = Visibility.Visible;
        }

        private void SavePhotographerToPictureobject(object sender, RoutedEventArgs e)
        {
            List<Photographer> phs = new List<Photographer>();
            foreach (Photographer ph in PhotgrapherGrid.SelectedItems)
            {
                phs.Add(ph);
            }
            ViewModel.MySql.UpdatePhotographerToPicture(phs, ViewModel.PicViewModel.CurrentPicture);
            ViewModel.PicViewModel.UpdatePhotographerId();
        }

        private void Search(object sender, RoutedEventArgs e)
        {
            _logger.LogDebug($"Search for {search_field.Text}");
            var search = search_field.Text.ToLower();
            if (search == "")
            {
                ViewModel.PicViewModel.CurrentPictureList = ViewModel.PicViewModel.PictureList;
                picture_list_box.PictureListBoxTemplate.ItemsSource = ViewModel.PicViewModel.PictureListAsImage;
                return;
            }
            ObservableCollection<Picture> resultPictures = new ObservableCollection<Picture>();
            foreach (Picture pic in ViewModel.PicViewModel.PictureList)
            {
                if (pic.Path.Contains(search))
                {
                    resultPictures.Add(pic);
                }
                else
                {
                    if (pic.Tags.FindIndex(x => x.Equals(search, StringComparison.OrdinalIgnoreCase)) != -1)
                    {
                        resultPictures.Add(pic);
                    }
                    else
                    {
                        if (pic.EXIFMetaData.Manufacturer.ToLower() == search || pic.EXIFMetaData.Model.ToLower() == search || pic.EXIFMetaData.Software.ToLower() == search || pic.EXIFMetaData.Flash.ToLower() == search || pic.EXIFMetaData.ColorSpace.ToLower() == search)
                        {
                            resultPictures.Add(pic);
                        }
                        else if (pic.IPTCMetaData.Title.ToLower() == search || pic.IPTCMetaData.Description.ToLower() == search || pic.IPTCMetaData.City.ToLower() == search)
                        {
                            resultPictures.Add(pic);
                        }
                        else
                        {
                            foreach (Photographer ph in pic.PhotograperList)
                            {
                                if (ph.FirstName.ToLower() == search || ph.LastName.ToLower() == search || ph.Email.ToLower() == search)
                                {
                                    resultPictures.Add(pic);
                                }
                            }
                        }
                    }
                }
            }

            if (resultPictures.Count > 0)
            {
                picture_list_box.PictureListBoxTemplate.ItemsSource = ViewModel.PicViewModel.PicturesToImages(resultPictures);
                ViewModel.PicViewModel.CurrentPictureList = resultPictures;
            }
            else
            {
                //empty
                //ViewModel.PicViewModel.CurrentPictureList = new ObservableCollection<Picture>();
                MessageBox.Show($"No Picture found for {search_field.Text}");
            }
        }

        private void AddNewPicture(object sender, RoutedEventArgs e)
        {
            ViewModel.AddNewPicture();
        }

        private void DeletePicture(object sender, RoutedEventArgs e)
        {
            ViewModel.DeletePicture();
        }
    }
}
