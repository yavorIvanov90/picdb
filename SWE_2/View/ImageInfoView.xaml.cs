﻿using SWE_2.Model;
using SWE_2.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWE_2.View
{
    /// <summary>
    /// Interaction logic for ImageInfoView.xaml
    /// </summary>
    public partial class ImageInfoView : Window
    {
        private readonly PictureViewModel _picViewModel;
        private readonly PhotographerViewModel _phViewModel;

        public ImageInfoView(PictureViewModel picViewModel, PhotographerViewModel phViewModel)
        {
            _picViewModel = picViewModel;
            _phViewModel = phViewModel;
            DataContext = _picViewModel;
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            e.Cancel = true;
        }

        private void Print(object sender, RoutedEventArgs e)
        {
            PrintDialog printDlg = new PrintDialog();
            printDlg.PrintVisual(grid, "User Control Printing.");
        }
    }
}
