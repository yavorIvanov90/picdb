﻿using SWE_2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using SWE_2.Model;

namespace SWE_2.View
{
    /// <summary>
    /// Interaction logic for PhotographerView.xaml
    /// </summary>
    public partial class PhotographerView : Window

    {

        public ICommand MyCommand { get; set; }
        public PhotographerViewModel _pViewModel;
        public PhotographerView(PhotographerViewModel pViewModel)
        {
            _pViewModel = pViewModel;
            DataContext = _pViewModel;
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            e.Cancel = true;
        }

        private void AddNewPhotographer(object sender, RoutedEventArgs e)
        {
            DateTime newDT = DateTime.Parse(txtBirthday.Text);
            Photographer ph = new Photographer(txtFirstName.Text, txtLastName.Text, newDT, txtEmail.Text);
            _pViewModel.AddOrUpdate(ph);
        }

        private void DeletePhotographer(object sender, RoutedEventArgs e)
        {
            if(txtId.Text != null && txtId.Text != "")
                _pViewModel.DeletePhotographer(Int32.Parse(txtId.Text));
        }

        private void ClearSelection(object sender, RoutedEventArgs e)
        {
            /*txtId.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtBirthday.Text = "";*/
            PhotgrapherGrid.SelectedIndex = -1;

        }
    }
}
