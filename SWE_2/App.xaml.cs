﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SWE_2.ViewModel;
using SWE_2.View;
namespace SWE_2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        private IConfiguration Configuration { get; set; }

        private readonly ServiceProvider _serviceCollection;
        public App()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<MainWindow>();
            serviceCollection.AddSingleton<MainWindowViewModel>();
            serviceCollection.AddSingleton<PhotographerViewModel>();
            serviceCollection.AddSingleton<PictureViewModel>();
            serviceCollection.AddSingleton<PictureListBox>();
            serviceCollection.AddSingleton<ImageInfoView>();
            serviceCollection.AddSingleton<TagsInfoView>();
            serviceCollection.AddSingleton<TagsInfoViewModel>();
            serviceCollection.AddSingleton<PhotographerView>();
            serviceCollection.AddSingleton<IConfiguration>(provider => Configuration);
            serviceCollection.AddSingleton<MySqlConnection>();
            serviceCollection.AddLogging(builder =>
           {
               builder.AddFilter("Microsoft", LogLevel.Error);
               builder.AddFilter("System", LogLevel.Warning);
               builder.AddFilter("MainWindow", LogLevel.Debug);
               builder.AddConsole();
               builder.AddEventLog();
               builder.SetMinimumLevel(LogLevel.Debug);
           });

            _serviceCollection = serviceCollection.BuildServiceProvider();
            _serviceCollection.GetService<ILoggerFactory>().CreateLogger<MainWindow>();
        }

        private void App_OnStartUp(object sender, StartupEventArgs args)
        {
            var builder = new ConfigurationBuilder()
         .SetBasePath(Directory.GetCurrentDirectory())
         .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            Configuration = builder.Build();

            var sql = _serviceCollection.GetService<MySqlConnection>();
            sql.Connection = Configuration.GetValue<String>("SQLConnection");
            sql.openConnection();
            var mainWindow = _serviceCollection.GetService<MainWindow>();
            mainWindow.Show();
        }
    }
}
