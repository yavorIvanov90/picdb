﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaData
{
    public class EXIFMetaData
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Software { get; set; }
        public string Flash { get; set; }
        public string ColorSpace { get; set; }
        //public List<string> Keywords { get; set; }

        public EXIFMetaData() { }
    }
}
