﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaData
{
    public class IPTCMetaData
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string City { get; set; }

    }
}
